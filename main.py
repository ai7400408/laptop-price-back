from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
import joblib

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:5174"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

# Load the trained model
model = joblib.load('laptopModel.pkl')

class PriceMeasurement(BaseModel):
    CompanyName: int
    ScreenResolution: int
    Cpu: int
    Ram: int
    Gpu: int
    OpSys: int
    TouchScreen: int
    MemoryType: int

class LaptopPrice(BaseModel):
    Price: float

@app.post("/laptop/price/")
async def predict_laptop_price(laptop: PriceMeasurement):
    try:
        data = [[laptop.CompanyName, laptop.ScreenResolution, laptop.Cpu, laptop.Ram, laptop.Gpu, laptop.OpSys, laptop.TouchScreen, laptop.MemoryType]]
        prediction = model.predict(data)
        
        return {prediction[0]}
    except Exception as e:
        print("Error prediction:", e)
        return {"error": "Prediction failed"}